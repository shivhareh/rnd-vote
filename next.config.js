/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  basePath: "/wips-vote",
  typescript:{
    ignoreBuildErrors: true,
  },
  experimental: {
    serverActions: {
      allowedOrigins: [
        "uat.rndapps.indianoil.in",
        "localhost",
        "rndapps.indianoil.in",
        "10.15.64.153",
      ],
    },
  },
};

module.exports = nextConfig;
