import Page from "@/components/Page";
import { HowToVote } from "@mui/icons-material";
import { haveAlreadyVoted } from "@/app/server-action";
import VotingForm from "@/components/VotingForm";
import { getServerSession } from "next-auth/next";
import { redirect } from "next/navigation";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";

export default async function Vote() {
  const session: any = await getServerSession(authOptions);
  const user = session?.user;
  if (!session) redirect("/api/auth/signin");

  const messages = {
    inactive: "The poll is not active.",
    ineligible: "You are ineligible to vote.",
    voted: "You have already voted.",
  };

  const alreadyVoted = await haveAlreadyVoted(user?.username);

  let pollStatus = "active";

  if (user?.gender !== "Female") {
    pollStatus = "ineligible";
  } else if (alreadyVoted) {
    pollStatus = "voted";
  }

  if (pollStatus === "active") {
    return (
      <Page title="WIPS Executive Committee Election" icon={HowToVote}>
        <VotingForm></VotingForm>
      </Page>
    );
  } else {
    return (
      <Page title="WIPS Executive Committee Election" icon={HowToVote}>
        {messages[pollStatus as keyof typeof messages]}
      </Page>
    );
  }
}
