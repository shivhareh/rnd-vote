"use server";
import { Employee } from "@/database/models";
import { WipsVote } from "@/database/models/WipsVote";
import { WipsVoteAuthorizedUser } from "@/database/models/WipsVoteAuthorizedUser";
import Op from "sequelize/lib/operators";

export default async function submitResponse(formData: any) {
  const { voterId, ec_members } = formData;
  const vote = {
    voterId: voterId,
    // coordinator: coordinator,
    ecMember1: ec_members[0],
    ecMember2: ec_members[1],
    ecMember3: ec_members[2],
    ecMember4: ec_members[3],
  };
  try {
    const submittedVote = await WipsVote.create(vote);
    return JSON.parse(JSON.stringify(submittedVote));
  } catch (e) {
    throw new Error("You have already voted.");
  }
}

export async function haveAlreadyVoted(username: string) {
  const voterDb = await WipsVote.findAll({
    where: {
      voterId: username,
    },
  });
  const voter = JSON.parse(JSON.stringify(voterDb));
  if (voter?.length > 0) {
    return true;
  } else {
    return false;
  }
}

export async function getAuthorizedUsers() {
  const voterDb = await WipsVoteAuthorizedUser.findAll();
  const users = JSON.parse(JSON.stringify(voterDb));
  return users;
}

export async function getCoordinatorVotesByUsername(username: string) {
  const votesCount = await WipsVote.count({
    where: {
      coordinator: username,
    },
  });
  return votesCount;
}

export async function getECMemberVotesByUsername(username: string) {
  const votesCount = await WipsVote.count({
    where: {
      [Op.or]: [
        {
          ecMember1: username,
        },
        {
          ecMember2: username,
        },
        {
          ecMember3: username,
        },
        {
          ecMember4: username,
        },
      ],
    },
  });
  return votesCount;
}
