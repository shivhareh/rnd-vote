import Page from "@/components/Page";
import { HowToVote, Poll } from "@mui/icons-material";
import {
  getAuthorizedUsers,
  getCoordinatorVotesByUsername,
  getECMemberVotesByUsername,
  haveAlreadyVoted,
} from "@/app/server-action";
import { getServerSession } from "next-auth/next";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import {
  COORDINATOR_CANDIDATES,
  EC_MEMBER_CANDIDATES,
} from "@/enums/constants";
import { Divider } from "@mui/material";
import { redirect } from "next/navigation";

export default async function Results() {
  const session: any = await getServerSession(authOptions);
  const user = session?.user;
  if (!session) redirect("/api/auth/signin");

  const authorizedUsers = await getAuthorizedUsers();

  const isAuthorized = authorizedUsers.some(
    (authUser: any) => authUser.username === user.username
  );

  // const coordinatorResults = await Promise.all(
  //   COORDINATOR_CANDIDATES.map(async (candidate) => {
  //     return {
  //       ...candidate,
  //       noOfVotes: await getCoordinatorVotesByUsername(candidate.username),
  //     };
  //   })
  // );

  const ecMembersResults = await Promise.all(
    EC_MEMBER_CANDIDATES.map(async (candidate) => {
      return {
        ...candidate,
        noOfVotes: await getECMemberVotesByUsername(candidate.username),
      };
    })
  );

  if (isAuthorized) {
    return (
      <Page title="WIPS Executive Committee Election Results" icon={Poll}>
        <div className=" flex w-1/2 flex-col gap-4 self-center bg-white p-8">
          {/* <h2 className="font-bold">Co-ordinator</h2>
          {coordinatorResults.map((coordinator: any) => {
            return (
              <>
                <div
                  key={coordinator.key}
                  className="flex justify-between gap-4"
                >
                  <div>{coordinator.name}</div>
                  <div>{coordinator.noOfVotes}</div>
                </div>
                <Divider />
              </>
            );
          })} */}
          <h2 className="font-bold">EC Members</h2>
          {ecMembersResults.map((candidate: any) => {
            return (
              <>
                <div key={candidate.key} className="flex justify-between gap-4">
                  <div>{candidate.name}</div>
                  <div>{candidate.noOfVotes}</div>
                </div>
                <Divider />
              </>
            );
          })}
        </div>
      </Page>
    );
  } else {
    return (
      <Page title="WIPS Executive Committee Election" icon={HowToVote}>
        <p>You are not authorized to view the results.</p>
      </Page>
    );
  }
}
