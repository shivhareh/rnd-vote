import type { Metadata } from "next";
import "@/globals.css";
import ThemeProvider from "@/next-theme-provider";
import ResponsiveLayout from "@/components/ResponsiveLayout";
import "@fontsource/poppins/700.css"; // Specify weight
import "@fontsource/noto-sans";
import { LocalizationProvider, AdapterDayjs } from "../core-components";
import "dayjs/locale/en-in";
import { Toaster } from "sonner";
import Provider from "@/context/client-provider";
import { getServerSession } from "next-auth/next";
import { authOptions } from "./api/auth/[...nextauth]/route";
import { redirect } from "next/navigation";
import { APP_TITLE } from "@/enums/constants";
import { Employee } from "@/database/models/";
import { WipsVote } from "@/database/models/WipsVote";
import { WipsVoteAuthorizedUser } from "@/database/models/WipsVoteAuthorizedUser";

export const metadata: Metadata = {
  title: APP_TITLE,
  description:
    "A request management system for all kinds of requests. Developed by Mr Harshvardhan Shivhare and Mr Aditya Raj.",
};

export const dynamic = "force-dynamic";

async function createInstance() {
  await WipsVote.sync({ force: true });
  await WipsVoteAuthorizedUser.sync({ force: true });
}

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  // await createInstance();

  const session: any = await getServerSession(authOptions);
  if (!session) redirect("/api/auth/signin");
  const user = session?.user;

  const sidebarContent = {
    itemsBadgeContent: {
      inbox: 0,
      admin: 0,
    },
  };

  return (
    <html lang="en">
      <body>
        <Provider session={session}>
          <ThemeProvider>
            <ResponsiveLayout sidebarContent={sidebarContent}>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                adapterLocale="en-in"
              >
                {children}
                <Toaster position="bottom-right" richColors closeButton />
              </LocalizationProvider>
            </ResponsiveLayout>
          </ThemeProvider>
        </Provider>
      </body>
    </html>
  );
}
