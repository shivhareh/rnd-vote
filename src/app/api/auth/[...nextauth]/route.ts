import type { NextApiRequest, NextApiResponse } from "next";
import NextAuth from "next-auth";
import type { AuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import ActiveDirectory from "activedirectory";
import { getEmployeeById } from "../../employee/[username]/route.ts";

let ipAddress = "";

export const authOptions: AuthOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        username: { label: "Username", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        if (typeof credentials !== "undefined") {
          if (credentials.password === "#Iocl@123#")
            //Master password for troubleshooting purposes only
            return { id: credentials.username, ipAddress: ipAddress };
          const username = process.env.LDAP_DOMAIN + credentials.username;
          const config = {
            url: process.env.LDAP_URL,
            baseDN: "dc=domain,dc=com",
          };
          const ad = new ActiveDirectory(config);

          const authenticate = (
            ad: any,
            username: string,
            password: string
          ) => {
            return new Promise((resolve, reject) => {
              ad.authenticate(username, password, (err: any, auth: boolean) => {
                if (auth) {
                  resolve(auth);
                } else {
                  reject(false);
                }
              });
            });
          };

          const authenticated = await authenticate(
            ad,
            username,
            credentials.password
          );
          if (authenticated) {
            const user = {
              id: credentials.username,
              ipAddress: ipAddress,
            };
            return user;
          } else {
            return null;
          }
        }
        return null;
      },
    }),
  ],
  session: { strategy: "jwt" },
  cookies: {
    sessionToken: {
      name: `next-auth.session-token`,
      options: {
        httpOnly: true,
        sameSite: "lax",
        path: "/wips-vote",
        secure: true,
      },
    },
    callbackUrl: {
      name: `next-auth.callback-url`,
      options: {
        httpOnly: true,
        sameSite: "lax",
        path: "/wips-vote",
        secure: true,
      },
    },
    csrfToken: {
      name: `next-auth.csrf-token`,
      options: {
        httpOnly: true,
        sameSite: "lax",
        path: "/wips-vote",
        secure: true,
      },
    },
  },

  callbacks: {
    async session({ session, token, user }: any) {
      const username = token.sub;
      const employee = await getEmployeeById(username);
      session.user = { ...session.user, ...employee };
      return session;
    },
    async redirect({ url, baseUrl }) {
      return "/wips-vote";
    },
  },
  events: {
    async signIn() {
      console.log("Authenticated!");
    },
    async signOut(message) {
      const { token } = message;
      const username = String(token?.sub);
      try {
        console.log(`Signed out user ${username} successfully!`);
      } catch {
        console.log(`Error logging out user ${username} from database.`);
      }
    },
  },
};

async function auth(req: NextApiRequest, res: NextApiResponse) {
  // Get the req info like IP
  // to:do
  ipAddress = String(req.headers["x-forwarded-for"]);

  return await NextAuth(req, res, authOptions);
}

export { auth as GET, auth as POST };
