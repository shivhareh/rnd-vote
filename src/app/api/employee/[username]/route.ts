import { sequelize } from "@/database/db";
import { Employee } from "@/database/models/";
import { QueryTypes } from "sequelize";

const QUERY_GET_CURRENT_ADDRESS_OF_EMPLOYEE =
  "SELECT PERSONAL_NO,CURRENT_ADDRESS_LINE1,CURRENT_ADDRESS_LINE2,CURRENT_CITY,CURRENT_DISTRICT FROM SAPMASTERDATA.SAP_EMPLOYEE_BASIC_DETAILS_IOCL WHERE PERSONAL_NO = :empId";

const getCurrentAddressOfEmployee = async (empId: any) => {
  if (!empId) {
    return null;
  }
  const result = await sequelize.query(QUERY_GET_CURRENT_ADDRESS_OF_EMPLOYEE, {
    replacements: { empId },
    type: QueryTypes.SELECT,
    logging: false,
    raw: false,
  });
  if (result?.length !== 1) {
    return null;
  }
  return result[0];
};

export async function getEmployeeById(username: string) {
  const employee = await Employee.findOne({
    where: {
      username,
    },
  });

  const jsonEmployee = JSON.parse(JSON.stringify(employee));

  const currentAddress = await getCurrentAddressOfEmployee(jsonEmployee.empNo);
  const emp = {
    ...jsonEmployee,
    sapData: currentAddress,
  };

  // @ts-ignore
  return emp;
}

export async function GET(
  request: Request,
  { params }: { params: { username: string } }
) {
  const res = await getEmployeeById(params.username);

  return Response.json(res);
}
