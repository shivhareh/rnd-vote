"use server";

import { Employee } from "@/database/models/";
import { STATUS_CODES } from "@/enums/enums";
import { Op } from "sequelize";

export async function getAllEmployees() {
  try {
    const employees = await Employee.findAll();
    const parsedEmpList = JSON.parse(JSON.stringify(employees));
    return parsedEmpList;
  } catch (e: any) {
    console.log(e);
    return {
      errorMessage: e?.message || "Something went wrong!",
      statusCode: STATUS_CODES.ERROR,
    };
  }
}

export async function GET() {
  const res = await getAllEmployees();

  return Response.json(res);
}
