"use client";

import { SessionProvider } from "next-auth/react";

export default function Provider({
  children,
  session,
}: {
  children: React.ReactNode;
  session: any;
}): React.ReactNode {
  return (
    <SessionProvider session={session} basePath={process.env.NEXTAUTH_URL}>
      {children}
    </SessionProvider>
  );
}
