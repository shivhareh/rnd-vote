import dayjs from "dayjs";
const getLastDateOfFinancialYear = () => {
  const today = dayjs();
  const month = today.month();
  if (month <= 2) {
    return dayjs().set("date", 31).set("month", 2).set("year", today.year());
  } else {
    return dayjs()
      .set("date", 31)
      .set("month", 2)
      .set("year", today.year() + 1);
  }
};

export { getLastDateOfFinancialYear };
