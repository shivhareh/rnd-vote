import { sequelize, DataTypes } from "@/database/db";

export const WipsVoteAuthorizedUser = sequelize.define(
  "WipsVoteAuthorizedUser",
  {
    username: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
  },
  {
    tableName: "WIPS_VOTE_AUTHORIZED_USERS",
    underscored: true,
    timestamps: false,
  }
);
