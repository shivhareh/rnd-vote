import { sequelize, DataTypes } from "@/database/db";

export const WipsVote = sequelize.define(
  "WipsVote",
  {
    voteId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    voterId: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    coordinator: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    ecMember1: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ecMember2: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ecMember3: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ecMember4: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    tableName: "WIPS_VOTE",
    underscored: true,
  }
);
