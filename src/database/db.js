import { Sequelize, DataTypes } from "sequelize";
import oracledb from "oracledb";

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "oracle",
    dialectModule: oracledb,
    logging: false, // to:do revert in poduction
  }
);

export const connectDB = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

export { DataTypes, sequelize };
