"use client";
import { useEffect } from "react";
import { useState } from "react";
import {
  ThemeProvider as MuiThemeProvider,
  theme as lightTheme,
  darkTheme,
  ThreeRowSkeleton,
} from "./core-components";
import { useTheme } from "next-themes";

export default function ThemeProvider({ children }: any) {
  const [mounted, setMounted] = useState(false);
  const { theme, setTheme } = useTheme();

  // useEffect only runs on the client, so now we can safely show the UI
  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) {
    return (
      <main className="flex flex-1 flex-col">
        <div className="relative my-8 flex w-full max-w-4xl flex-col justify-center self-center rounded-3xl bg-white p-10 dark:bg-slate-700">
          <ThreeRowSkeleton />
        </div>
      </main>
    );
  }

  const themeObject: any = theme === "light" ? lightTheme : darkTheme;
  return <MuiThemeProvider theme={themeObject}>{children}</MuiThemeProvider>;
}
