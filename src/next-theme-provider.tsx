"use client";
// import { ThemeProvider as NextThemeProvider } from "next-themes";
import MUIThemeProvider from "./mui-theme-provider";
import dynamic from "next/dynamic";
const NextThemeProvider = dynamic(
  () => import("next-themes").then((mod) => mod.ThemeProvider),
  { ssr: false }
);

export default function ThemeProvider({ children }: any) {
  return (
    <NextThemeProvider enableSystem={false} attribute="class">
      <MUIThemeProvider>{children}</MUIThemeProvider>
    </NextThemeProvider>
  );
}
