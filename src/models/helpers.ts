const getEmplpoyeeOptions = (empList: any) => {
  if (!empList) {
    return [];
  }
  empList.sort((a: any, b: any) => a.name.localeCompare(b.name));
  return empList;
};

const getEmployeeOptionLabel = (option: any) => {
  const name = option?.name || "",
    designation = option?.designation || "NA",
    username = option?.username || "NA",
    department = option?.department || "NA";

  return option ? `${name} [${designation}] [${username}] - ${department}` : "";
};
const isEmployeeOptionEqualToValue = (option: any, value: any) =>
  option.username === value.username;

/**
 * Retrieves a list of employees with the highest grade within a given department designation.
 *
 * @param {Array} empList - An array of employee objects.
 * @param {string} dept - The department designation to filter by. Default is an empty string.
 *
 * @returns {Array} - An array of employee objects with the highest grade within the specified department designation.
 */
const getHodsByDesignation = (
  empList: any[],
  dept: string = ""
): Array<any> => {
  // Check if the input employee list is not defined or empty and return an empty array in that case.
  if (!empList) {
    return [];
  }

  // Create a copy of the employee list initially.
  let filteredEmpList = getActiveRNDEmployees(empList);

  // If a department designation is specified, filter the employees based on the designation.
  if (dept) {
    filteredEmpList = getActiveRNDEmployees(empList).filter((emp: any) =>
      emp.designation?.includes(dept)
    );
  }

  // Extract an array of employee grades from the filtered list.
  const grades = filteredEmpList.map((emp: any) => emp.grade);

  // Find the maximum grade from the array of grades.
  let maxGrade = grades[0];
  grades.forEach((grade: any) => {
    if (grade.localeCompare(maxGrade) > 0) maxGrade = grade;
  });

  // Filter and retrieve employees with the highest grade.
  const hods = filteredEmpList.filter((emp: any) => emp.grade === maxGrade);

  // Return the list of employees with the highest grade within the specified department designation.
  return hods;
};

const getActiveRNDEmployees = (options: any) =>
  options.filter(
    (opt: any) => opt.rndStatus === "ACTIVE" || opt.rndStatus === "INCLUDED"
  );

const getRecommendingEmployees = (options: any, loggedInUser: any) =>
  getActiveRNDEmployees(options).filter(
    (opt: any) =>
      opt.grade?.localeCompare(loggedInUser.grade) >= 0 &&
      opt.username !== loggedInUser.username
  );

const RND_HELPDESK_USER = {
  uniqueId: 99900001,
  empNo: 99900001,
  emailId: "RNDHELPDESK@INDIANOIL.IN",
  name: "RND_HELPDESK",
  notAnEmployee: true,
  designation: "NA",
  departmentCode: "D1213",
  department: "Info. Systems",
  grade: "",
};

const RND_CISF_AC = {
  uniqueId: 99900002,
  empNo: 99900002,
  emailId: "RND_CISF_AC@INDIANOIL.IN",
  name: "RND_CISF_AC",
  notAnEmployee: true,
  designation: "NA",
  departmentCode: "D1209",
  department: "Human Resource",
  grade: "",
};

const RND_APT = {
  uniqueId: 99900003,
  empNo: 99900003,
  emailId: "RND_APT@indianoil.in",
  name: "RND_APT",
  notAnEmployee: true,
  designation: "NA",
  departmentCode: "D1213",
  department: "Info. Systems",
  grade: "",
};

export {
  getEmplpoyeeOptions,
  getEmployeeOptionLabel,
  isEmployeeOptionEqualToValue,
  getHodsByDesignation,
  getRecommendingEmployees,
  getActiveRNDEmployees,
  RND_HELPDESK_USER,
  RND_CISF_AC,
  RND_APT,
};
