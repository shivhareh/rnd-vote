"use server";
import { Employee, RmsProcessAdmin } from "@/database/models/";
import { Op } from "sequelize";
import { STATUS_CODES } from "./enums";

export const getEmployeesList = async () => {
  try {
    const empList = await Employee.findAll({
      where: {
        dateOfJoining: {
          [Op.ne]: null,
        },
      },
    });
    return JSON.parse(JSON.stringify(empList));
  } catch (e: any) {
    console.log(e);
    return {
      errorMessage: e?.message || "Something went wrong!",
      statusCode: STATUS_CODES.ERROR,
    };
  }
};

export const getISDeptEmployees = async () => {
  try {
    const empList = await Employee.findAll({
      where: {
        dateOfJoining: {
          [Op.ne]: null,
        },
        departmentCode: "D1213", // IS Department
      },
    });
    return JSON.parse(JSON.stringify(empList));
  } catch (e: any) {
    console.log(e);
    return {
      errorMessage: e?.message || "Something went wrong!",
      statusCode: STATUS_CODES.ERROR,
    };
  }
};

export const getProcessingAuthority = async (
  requestType: any,
  processStepOrder: any
) => {
  try {
    const usernameList = await RmsProcessAdmin.findAll({
      where: {
        requestType,
        processStepOrder,
      },
      attributes: ["username"],
    });

    const usernameListParsed = JSON.parse(JSON.stringify(usernameList)).map(
      (user: any) => user.username
    );

    const empList = await Employee.findAll({
      where: {
        username: usernameListParsed,
      },
    });
    return JSON.parse(JSON.stringify(empList));
  } catch (e: any) {
    console.log(e);
    return {
      errorMessage: e?.message || "Something went wrong!",
      statusCode: STATUS_CODES.ERROR,
    };
  }
};
