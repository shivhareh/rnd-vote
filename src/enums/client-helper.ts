import { getEmployeeOptionLabel } from "@/models/helpers";
import { InputTypes } from "./input-types";
import {
  FormAutocomplete,
  FormDatePicker,
  FormMobileTimePicker,
  FormTextField,
  SingleFileUpload,
} from "@/core-components";

export const getEmployeeLabelByUsername = (
  username: any,
  employeeList: any
) => {
  const index = employeeList.findIndex((emp: any) => emp.username === username);

  if (index > -1) {
    return getEmployeeOptionLabel(employeeList[index]);
  }
  return username;
};

export function romanize(num: any) {
  if (isNaN(num)) return NaN;
  let digits: any = String(+num).split(""),
    key = [
      "",
      "C",
      "CC",
      "CCC",
      "CD",
      "D",
      "DC",
      "DCC",
      "DCCC",
      "CM",
      "",
      "X",
      "XX",
      "XXX",
      "XL",
      "L",
      "LX",
      "LXX",
      "LXXX",
      "XC",
      "",
      "I",
      "II",
      "III",
      "IV",
      "V",
      "VI",
      "VII",
      "VIII",
      "IX",
    ],
    roman = "",
    i = 3;
  while (i--) roman = (key[+digits.pop() + i * 10] || "") + roman;
  return Array(+digits.join("") + 1).join("M") + roman;
}

export const showElement = (field: any, watchedFields: any) => {
  if (!field?.dynamicRender) {
    return true;
  }
  if (field.dependsUpon) {
    const curValue = watchedFields.find(
      (wf: any) => wf.key === field.dependsUpon.key
    )?.value;

    if (
      field.dependsUpon.isValueEqualsCurValue(curValue, field.dependsUpon.value)
    ) {
      return true;
    }
    return false;
  }
};

const getNamesToBeWatched = (formFields: any) => {
  let watchedFields: any[] = [];
  Object.values(formFields).forEach(async (field: any) => {
    if (field?.dependsUpon) {
      watchedFields.push(field?.dependsUpon.key);
    }
    if (field?.dynamicValue) {
      watchedFields.push(field?.dynamicValueDependsUpon.key);
    }
  });
  watchedFields = Array.from(new Set(watchedFields));
  return watchedFields;
};

export const getWatchedFields = (formFields: any, watch: any) => {
  const watchedFields: any = [];
  getNamesToBeWatched(formFields).forEach((wf) => {
    watchedFields.push({
      key: wf,
      value: watch(wf),
    });
  });
  return watchedFields;
};

export const getFormDefaultValues = (formFields: any) => {
  const defaultValues: any = {};
  Object.values(formFields).forEach((field: any) => {
    if (field?.addableValuesField) {
      let inputGroupDefaultValues: any = {};

      field.inputGroup.forEach((ig: any) => {
        inputGroupDefaultValues[ig["key"]] = ig.defaultValue;
      });

      defaultValues[field["key"]] = [...Array(field.minValue)].map(
        () => inputGroupDefaultValues
      );
    } else {
      defaultValues[field["key"]] = field.defaultValue;
    }
  });

  return defaultValues;
};

export const getInputType = (inputType: string) => {
  switch (inputType) {
    case InputTypes.AutoComplete:
      return FormAutocomplete;
    case InputTypes.TextField:
      return FormTextField;
    case InputTypes.DatePicker:
      return FormDatePicker;
    case InputTypes.MobileTimePicker:
      return FormMobileTimePicker;
    case InputTypes.FileUpload:
      return SingleFileUpload;
    default:
      return "div";
  }
};

export const getAutocompleteOptions = (
  field: any,
  watchedFields: any,
  dynamicOptions: any
) => {
  if (field.type !== InputTypes.AutoComplete) {
    return undefined;
  }
  if (field?.inputTypeProps?.options) {
    return field?.inputTypeProps?.options;
  }
  if (field?.hasDynamicOptions) {
    if (field?.filterOptions && field?.getOptions) {
      if (field?.optionsDependsUpon) {
        const curValue = watchedFields.find(
          (wf: any) => wf.key === field.optionsDependsUpon.key
        )?.value;
        return field.filterOptions(
          field.getOptions(
            dynamicOptions.find((option: any) => option.field === field.key)
              ?.options
          ),
          curValue
        );
      }
      return field.filterOptions(
        field.getOptions(
          dynamicOptions.find((option: any) => option.field === field.key)
            ?.options
        )
      );
    }
    if (field.getOptions) {
      return field.getOptions(
        dynamicOptions.find((option: any) => option.field === field.key)
          ?.options
      );
    }
  }
  return null;
};

export const disableElement = (field: any, watchedFields: any) => {
  if (!field?.hasDynamicOptions) {
    return false;
  }
  if (field.optionsDependsUpon) {
    const curValue = watchedFields.find(
      (wf: any) => wf.key === field.optionsDependsUpon.key
    )?.value;
    if (field.optionsDependsUpon?.inValidCurrentValue(curValue)) {
      return true;
    }
    return false;
  }
};

export const getNamesWithDynamicValuesAndTheirDependents = (
  formFields: any
) => {
  const formFieldDynamicValue = Object.keys(formFields).filter(
    (key: any) => formFields[key]?.dynamicValue
  );
  const ffDependentUpon = formFieldDynamicValue.map((ff: any) => ({
    key: ff,
    dependsUpon: formFields[ff].dynamicValueDependsUpon.key,
    getValue: formFields[ff].dynamicValueDependsUpon.getValue,
  }));

  return ffDependentUpon;
};
