export const NAV_BAR_HEIGHT = 96;
export const MAX_CHARACTERS_ALLOWED = 1800;
export const MIN_INPUT_WIDTH = 360;

export const APP_TITLE = "R&D VOTE";

export const COORDINATOR_CANDIDATES = [
  {
    key: "coordinator_1",
    name: "Ms Anil Yadav, CRM (CCP)",
    username: "00225682",
  },
  {
    key: "coordinator_2",
    name: "Ms Aparna Bhargava, CPM (New Campus)",
    username: "00030325",
  },
  {
    key: "coordinator_3",
    name: "Ms Kuljeet Kaur, DGM (IS)",
    username: "00225637",
  },
];

export const EC_MEMBER_CANDIDATES = [
  // {
  //   key: "ec_candidate_1",
  //   name: "Ms Aparna Bhargava, CPM (New Campus)",
  //   username: "00030325",
  // },
  {
    key: "ec_candidate_2",
    name: "Ms Fariha Saleem, AMR (Fuels & Additives)",
    username: "00513529",
  },
  {
    key: "ec_candidate_3",
    name: "Ms Hima Bindu, RM (CCP)",
    username: "00502398",
  },
  {
    key: "ec_candidate_4",
    name: "Ms Pragya, AMR (Industrial Bio-Technology)",
    username: "00511868",
  },
  {
    key: "ec_candidate_5",
    name: "Ms Priyanka Luthra, Research Officer (Petrochemicals & Polymers)",
    username: "00225761",
  },
  {
    key: "ec_candidate_6",
    name: "Ms Shikha Saluja, AMR (CCP)",
    username: "00512845",
  },
  {
    key: "ec_candidate_7",
    name: "Ms Simmi Dhiman, AM (Finance)",
    username: "00084574",
  },
  {
    key: "ec_candidate_8",
    name: "Ms Sweety Rathi, AMR (MWO)",
    username: "00511768",
  },
];
