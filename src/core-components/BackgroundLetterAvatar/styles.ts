import { css } from "@emotion/react";

const classes = {
  container: (theme: any) =>
    css({
      label: "container",
    }),
  wrapper: css({
    label: "wrapper",
  }),
};
export default classes;
