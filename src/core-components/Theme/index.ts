import { ThemeProvider, createTheme } from "@mui/material/styles";

let iocTheme = createTheme({
  palette: {
    primary: {
      main: "#F37022",
      contrastText: "#FFFFFF",
    },
    secondary: {
      main: "#03174F",
    },
    background: {
      paper: "#fcfcfd",
    },
  },
  typography: {
    fontFamily: [
      "Noto Sans",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
    },
  },
});

iocTheme = createTheme(iocTheme, {
  // Custom colors created with augmentColor go here
  palette: {
    indigo: iocTheme.palette.augmentColor({
      color: {
        main: "#4B0082",
      },
      name: "indigo",
    }),
    golden: iocTheme.palette.augmentColor({
      color: {
        main: "#fcbf49",
      },
      name: "golden",
    }),
  },
});

let darkTheme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#F37022",
      contrastText: "#FFFFFF",
    },
    secondary: {
      // main: "#03174F",
      main: "#2196f3",
    },
    background: {
      paper: "#334155",
    },
    // background: {
    //   paper: "#000000BF",
    // },
  },
  typography: {
    fontFamily: [
      "Noto Sans",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
    },
  },
});

darkTheme = createTheme(darkTheme, {
  // Custom colors created with augmentColor go here
  palette: {
    indigo: iocTheme.palette.augmentColor({
      color: {
        main: "#4B0082",
      },
      name: "indigo",
    }),
    golden: iocTheme.palette.augmentColor({
      color: {
        main: "#fcbf49",
      },
      name: "golden",
    }),
  },
});

export { ThemeProvider, createTheme, iocTheme as theme, darkTheme };
