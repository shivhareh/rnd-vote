/** @jsxImportSource @emotion/react */
/**
 *
 * TextField Component
 *
 */
import { SxProps } from "@mui/material";
import { TextField as MuiTextField } from "@mui/material";
import classes from "./styles";
import messages from "./messages";

type DataProps = {
  /** Adds inline css property */
  style?: any;

  /** Label */
  label: string;

  /** true if error */
  error?: boolean;

  /** text below the text field */
  helperText?: string;

  value?: string;

  type?: string;

  autoComplete?: string;

  multiline?: boolean;

  rows?: number;
};

type ActionProps = {
  onChange?: (e: any) => void;
};

type DefaultProps = {
  /** Overrides sx properties of mui components*/
  sx?: SxProps;
};

export type Props = ActionProps & DataProps & DefaultProps;

function TextField(props: Props) {
  const { sx = {}, style } = props;
  return (
    <MuiTextField
      size="medium"
      variant="outlined"
      {...props}
      sx={{ width: "100%", ...props.sx }}
    />
  );
}

export default TextField;
