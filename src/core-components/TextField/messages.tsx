/**
 * TextField Messages
 *
 * This contains all the text for the TextField Component
 *
 */

export const scope = "src.component.TextField";

const messages = {
  header: "Test",
};

export default messages;
