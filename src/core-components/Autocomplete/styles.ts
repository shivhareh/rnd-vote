import { css } from "@emotion/react";

const classes = {
  container: (theme: any) =>
    css({
      label: "container",
      width: "100%",
    }),
  wrapper: css({
    label: "wrapper",
  }),
  warning: (theme: any) =>
    css({
      label: "warning",
      color: theme.palette.warning.main,
      display: "flex",
      alignItems: "center",
    }),
  warningIcon: css({
    label: "warningIcon",
    fontSize: 14,
    marginRight: 4,
  }),
};
export default classes;
