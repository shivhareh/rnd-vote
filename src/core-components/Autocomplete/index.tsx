/** @jsxImportSource @emotion/react */

/**
 *
 * Autocomplete Component
 *
 */
import { Autocomplete as MuiAutocomplete, SxProps } from "@mui/material";
import TextField from "../TextField";
import classes from "./styles";
import WarningIcon from "@mui/icons-material/Warning";

type DefaultProps = {
  error?: boolean;
  helperText?: string | any;
  size?: "small" | "medium";
};

type DataProps = {
  label: string;
  options: Array<any>;
  sx?: SxProps;
  value: any;
  textFieldSx?: SxProps;
  defaultValue?: any;
  inputValue?: any;
  warning?: string;
};

type ActionProps = {
  onChange: (event: any, value: any, reason: any) => void;
  getOptionLabel?: (option: any) => string;
  isOptionEqualToValue?: (option: any, value: any) => boolean;
  onInputChange?: (event: any, value: any, reason: any) => void;
};
export type Props = ActionProps & DataProps & DefaultProps;

function Autocomplete(props: Props) {
  const {
    error = false,
    helperText = "",
    sx = {},
    size = "medium",
    defaultValue,
    label,
    options,
    value,
    textFieldSx = {},
    onChange,
    warning,
    ...restMuiAutocompleteProps
  } = props;

  let updatedHelperText = helperText;
  if (!helperText && warning) {
    updatedHelperText = (
      <div css={classes.warning}>
        <WarningIcon css={classes.warningIcon} />
        <>{warning}</>
      </div>
    );
  }
  return (
    <div css={classes.container}>
      <MuiAutocomplete
        size={size}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            error={error}
            helperText={updatedHelperText}
            sx={textFieldSx}
          />
        )}
        options={options}
        onChange={onChange}
        value={value}
        sx={{ width: "100%", ...sx }}
        fullWidth
        defaultValue={defaultValue}
        {...restMuiAutocompleteProps}
      />
    </div>
  );
}

export default Autocomplete;
