/** @jsxImportSource @emotion/react */

import * as React from "react";
import { Button, IconButton } from "../MuiComponents";
import classes from "./styles";
import messages from "./messages";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";

export const VALID_FILE_EXTENSIONS = [
  ".pdf",
  ".png",
  ".jpg",
  ".jpeg",
  ".xls",
  ".xlsx",
  ".doc",
  ".docx",
  ".ppt",
  ".pptx",
];

function SingleFileUpload(props: any) {
  const { selectedFile, handleUploadFile, onRemoveFile } = props;

  return (
    <div css={classes.container}>
      <div css={classes.wrapper}>
        {!selectedFile?.name ? (
          <div css={classes.dropzone}>
            <Button
              variant="outlined"
              component="label"
              css={classes.uploadButton}
              startIcon={<CloudUploadIcon />}
            >
              {props?.label || messages.upload}
              <input
                hidden
                // multiple
                type="file"
                accept={VALID_FILE_EXTENSIONS.join(",")}
                onChange={(e: any) => {
                  handleUploadFile(e.target.files[0]);
                }}
              />
            </Button>
          </div>
        ) : (
          <div css={classes.fileList}>
            <div css={classes.wrap}>
              <div css={classes.fileName}>{selectedFile.name}</div>
              <IconButton
                aria-label="delete"
                color="primary"
                css={classes.deleteIcon}
                onClick={() => onRemoveFile()}
                // disabled={disableDelete}
              >
                <HighlightOffIcon fontSize="small" />
              </IconButton>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default SingleFileUpload;
