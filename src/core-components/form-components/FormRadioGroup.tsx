import * as React from "react";
import { RadioGroup } from "../MuiComponents";
import { Controller } from "react-hook-form";
import ErrorIcon from "@mui/icons-material/Error";

function FormRadioGroup(props: any) {
  const { name, control, rules, error, children, ...radioGroupProps } = props;
  let errorMessage = error?.message || "";
  if (error?.type === "required" && !error?.message) {
    errorMessage = "Please select a coodinator.";
  }
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }: any) => {
        const { value, onChange, onBlur, ref, ...restFields } = field;
        return (
          <div style={{ display: "flex", flexFlow: "column" }}>
            <RadioGroup
              {...restFields}
              value={value}
              onChange={(event: any, value: string, reason: any) =>
                onChange(value)
              }
              {...radioGroupProps}
            >
              {children}
            </RadioGroup>
            {!!error && (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  marginTop: 2,
                }}
              >
                <ErrorIcon color="error" fontSize="small" />
                <div
                  style={{
                    fontSize: "smaller",
                    fontWeight: "bold",
                    color: "#d32f2f",
                    marginLeft: 8,
                  }}
                >
                  {errorMessage}
                </div>
              </div>
            )}
          </div>
        );
      }}
    />
  );
}

export default FormRadioGroup;
