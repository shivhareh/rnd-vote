import * as React from "react";
import { DatePicker } from "../MuiComponents";
import { Controller } from "react-hook-form";

function FormDatePicker(props: any) {
  const {
    name,
    control,
    error,
    rules,
    disablePast = true,
    ...datePickerProps
  } = props;
  let errorMessage = error?.message || "";
  if (error?.type === "required" && !error?.message) {
    errorMessage = "This field is required!";
  }
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }: any) => {
        const { value, onChange, onBlur, ref, ...restFields } = field;
        return (
          <DatePicker
            {...restFields}
            value={value}
            onChange={(value: any) => onChange(value)}
            {...datePickerProps}
            disablePast={disablePast}
            sx={{ width: "100%", ...props.sx }}
            // error={!!error}
            // helperText={errorMessage}
          />
        );
      }}
    />
  );
}

export default FormDatePicker;
