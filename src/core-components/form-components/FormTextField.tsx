import * as React from "react";
import TextField, { Props as TextFieldProps } from "../TextField";
import { Controller } from "react-hook-form";

const invalidStringMessage = "Please dont use special characters!";
const invalidStringFormat = /[`{};'"|<>~]/;
// const invalidStringFormat = /[`{};'"|<>\/~]/;

function FormTextField(props: any) {
  const { name, control, error, rules, ...textFieldProps } = props;
  let errorMessage = error?.message || "";
  if (error?.type === "required" && !error?.message) {
    errorMessage = "This field is required!";
  }
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        ...rules,
        validate: {
          ...rules?.validate,
          validInput: (input: string) =>
            input
              ? !invalidStringFormat.test(input) || invalidStringMessage
              : true,
        },
      }}
      render={({ field }: any) => {
        const { value, onChange, onBlur, ref, ...restFields } = field;
        return (
          <TextField
            {...restFields}
            value={value}
            onChange={(value: any) => onChange(value)}
            {...textFieldProps}
            error={!!error}
            helperText={errorMessage}
          />
        );
      }}
    />
  );
}

export default FormTextField;
