import * as React from "react";
import Autocomplete, { Props as AutocompleteProps } from "../Autocomplete";
import { Controller } from "react-hook-form";

function FormAutocomplete(props: any) {
  const { name, control, rules, error, ...autoCompleteProps } = props;
  let errorMessage = error?.message || "";
  if (error?.type === "required" && !error?.message) {
    errorMessage = "This field is required!";
  }
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }: any) => {
        const { value, onChange, onBlur, ref, ...restFields } = field;
        return (
          <Autocomplete
            {...restFields}
            value={value}
            onChange={(event: any, value: string, reason: any) =>
              onChange(value)
            }
            {...autoCompleteProps}
            error={!!error}
            helperText={errorMessage}
          />
        );
      }}
    />
  );
}

export default FormAutocomplete;
