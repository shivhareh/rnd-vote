import * as React from "react";
import { Checkbox } from "../MuiComponents";
import { Controller } from "react-hook-form";

function FormCheckbox(props: any) {
  const { name, control, rules, error, children, ...checkboxProps } = props;
  let errorMessage = error?.message || "";
  if (error?.type === "required" && !error?.message) {
    errorMessage = "This field is required!";
  }
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }: any) => {
        const { value, onChange, onBlur, ref, ...restFields } = field;
        return (
          <Checkbox
            {...restFields}
            value={value}
            onChange={(event: any, value: string, reason: any) =>
              onChange(value)
            }
            {...checkboxProps}
          >
            {children}
          </Checkbox>
        );
      }}
    />
  );
}

export default FormCheckbox;
