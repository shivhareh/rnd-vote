/** @jsxImportSource @emotion/react */

/**
 *
 * DataGrid Component
 *
 */
import { SxProps } from "@mui/material";
import classes from "./styles";
import messages from "./messages";
import {
  DataGrid as MUIDataGrid,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
  GridToolbar,
} from "@mui/x-data-grid";

type DataProps = {
  /** Adds inline css property */
  style?: any;
};

type ActionProps = {};

type DefaultProps = {
  /** Overrides sx properties of mui components*/
  sx?: SxProps;
  rows: any;
  columns: any;
  slots?: any;
  columnVisibilityModel?: any;
  onColumnVisibilityModelChange?: any;
  slotProps?: any;
  initialState?: any;
  pageSizeOptions?: any;
  getRowHeight?: any;
};

export type Props = ActionProps & DataProps & DefaultProps;

function DataGrid(props: Props) {
  const {
    sx = {},
    style,
    rows,
    columns,
    slots,
    columnVisibilityModel,
    onColumnVisibilityModelChange,
    slotProps,
    initialState,
    ...rest
  } = props;
  let defaultSlots = undefined;
  if (!slots) {
    defaultSlots = {
      toolbar: DefaultToolbar,
    };
  }

  return (
    <div css={classes.container} style={style}>
      <MUIDataGrid
        rows={rows}
        columns={columns}
        autoHeight
        getRowHeight={() => "auto"}
        sx={{
          "&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": {
            py: "8px",
          },
          "&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
            py: "12px",
          },
          "&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
            py: "16px",
          },
          "& .MuiDataGrid-columnHeaderTitle": {
            textOverflow: "clip",
            whiteSpace: "break-spaces",
            lineHeight: 1.5,
          },
        }}
        getRowSpacing={(params: any) => ({
          top: 5,
          bottom: 5,
        })}
        css={[classes.grid, classes.margin]}
        columnHeaderHeight={112}
        slots={slots || defaultSlots}
        slotProps={slotProps}
        columnVisibilityModel={columnVisibilityModel}
        onColumnVisibilityModelChange={onColumnVisibilityModelChange}
        initialState={initialState}
        {...rest}
      />
    </div>
  );
}

function DefaultToolbar(props: any) {
  return (
    <GridToolbarContainer>
      <GridToolbarColumnsButton />
      <GridToolbarFilterButton />
      <GridToolbarDensitySelector />
      <GridToolbarExport />
    </GridToolbarContainer>
  );
}

export default DataGrid;
export {
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
  GridToolbar,
};

export type { GridColDef, GridRowsProp } from "@mui/x-data-grid";
