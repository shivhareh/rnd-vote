/**
 * DataGrid Messages
 *
 * This contains all the text for the DataGrid Component
 *
 */

export const scope = "src.component.DataGrid";

const messages = {
  header: "DataGrid",
};

export default messages;
