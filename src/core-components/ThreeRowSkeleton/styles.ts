const classes = {
  container: {
    width: "100%",
    display: "flex",
    flexFlow: "column",
  },
  skeleton: {
    margin: 1,
    borderRadius: 1,
  },
};

export default classes;
