"use client";
import { useState, forwardRef } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@/core-components";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";

const Transition = forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

type Props = {
  title: string;
  desc: string;
  onClose?: any;
  onSubmit?: any;
  children?: any;
  open: boolean;
  submitting?: boolean;
};

export default function AlertDialogSlide(props: Props) {
  return (
    <>
      <Dialog
        open={props.open}
        TransitionComponent={Transition}
        keepMounted
        onClose={props.onClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle sx={{ paddingBottom: 0 }}>{props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {props.desc}
          </DialogContentText>
          <DialogContentText
            id="alert-dialog-slide-description"
            sx={{ marginTop: 4 }}
            component={"div"}
          >
            {props.children}
          </DialogContentText>
        </DialogContent>
        {(props.onSubmit || props.onClose) && (
          <DialogActions>
            {props.onClose && (
              <Button
                onClick={props.onClose}
                color="secondary"
                variant="outlined"
              >
                Cancel
              </Button>
            )}
            {props.onSubmit && (
              <Button
                onClick={props.onSubmit}
                variant="contained"
                loading={props?.submitting}
              >
                Submit
              </Button>
            )}
          </DialogActions>
        )}
      </Dialog>
    </>
  );
}
