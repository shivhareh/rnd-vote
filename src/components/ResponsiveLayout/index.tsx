"use client";

import MyDrawer from "@/components/Drawer";
import NavBar from "@/components/NavBar";
import Footer from "@/components/Footer";
import { NAV_BAR_HEIGHT } from "@/enums/constants";
import { useState } from "react";
import useMediaQuery from "@mui/material/useMediaQuery/useMediaQuery";

export default function ResponsiveLayout({
  children,
  sidebarContent,
}: {
  children: React.ReactElement;
  sidebarContent: any;
}) {
  const [openDesktopDrawer, setOpenDesktopDrawer] = useState(true);
  const [openMobileDrawer, setOpenMobileDrawer] = useState(false);

  const isSmallScreen = useMediaQuery((theme: any) =>
    theme.breakpoints.down("sm")
  );

  const handleDrawerToggle = () => {
    setOpenDesktopDrawer(!openDesktopDrawer);
    setOpenMobileDrawer(!openMobileDrawer);
  };

  return (
    <div className="flex min-h-screen flex-col">
      <NavBar
        handleDrawerToggle={handleDrawerToggle}
        height={NAV_BAR_HEIGHT}
        isSmallScreen={isSmallScreen}
      ></NavBar>
      <div className="flex flex-1">
        <MyDrawer
          openDesktop={openDesktopDrawer}
          openMobile={openMobileDrawer}
          handleDrawerToggle={handleDrawerToggle}
          isSmallScreen={isSmallScreen}
          itemsBadgeContent={sidebarContent.itemsBadgeContent}
        ></MyDrawer>
        <div
          className={`flex flex-grow flex-col justify-between bg-slate-100 dark:bg-slate-800`}
        >
          <div className="flex flex-1">{children}</div>
          <Footer />
        </div>
      </div>
    </div>
  );
}
