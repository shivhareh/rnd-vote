"use client";
import * as React from "react";
import { styled } from "@mui/material/styles";
import MuiDrawer from "@mui/material/Drawer";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Image from "next/image";
import ioclRndLogo from "/public/iocl-rnd.png";
import ioclRndLogoWhite from "/public/iocl-rnd-white.png";
import Link from "next/link";
import { drawerItems } from "@/components/Drawer/items";
import Icon from "@mui/material/Icon";
import { useTheme } from "next-themes";
import { Badge } from "@mui/material";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const MiniDrawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

export default function MyDrawer({
  openDesktop,
  openMobile,
  handleDrawerToggle,
  isSmallScreen,
  itemsBadgeContent,
}) {
  const { theme, setTheme } = useTheme();

  const DrawerHeader = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    // ...theme.mixins.toolbar,
  }));
  const drawer = (
    <List>
      {drawerItems.map((item, index) => (
        <React.Fragment key={index}>
          <ListItem disablePadding sx={{ display: "block" }}>
            <Link href={item.link}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: open ? "initial" : "center",
                  px: 2.5,
                }}
                onClick={isSmallScreen ? handleDrawerToggle : () => null}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: open ? 3 : "auto",
                    justifyContent: "center",
                  }}
                >
                  <Badge
                    badgeContent={itemsBadgeContent[item.badgeKey]}
                    color="error"
                    invisible={!item.showBadge}
                  >
                    <Icon component={item.icon} />
                  </Badge>
                </ListItemIcon>
                <ListItemText
                  primary={item.name}
                  sx={{ opacity: open ? 1 : 0 }}
                />
              </ListItemButton>
            </Link>
          </ListItem>
          {(index === 2 || index === 2 || index === 4) && <Divider />}
        </React.Fragment>
      ))}
    </List>
  );

  return (
    <>
      {isSmallScreen ? (
        <MuiDrawer
          variant="temporary"
          open={openMobile}
          onClose={handleDrawerToggle}
          sx={{
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          <DrawerHeader className="h-24">
            <Toolbar>
              <Link href="/">
                <Image
                  src={theme === "dark" ? ioclRndLogoWhite : ioclRndLogo}
                  alt="IOCL R&D Logo"
                ></Image>
              </Link>
            </Toolbar>
          </DrawerHeader>
          <Divider />
          {drawer}
        </MuiDrawer>
      ) : (
        <MiniDrawer
          variant="permanent"
          open={openDesktop}
          // onMouseEnter={handleDrawerToggle}
          // onMouseLeave={handleDrawerToggle}
        >
          <DrawerHeader className="mb-24" />
          {drawer}
        </MiniDrawer>
      )}
    </>
  );
}
