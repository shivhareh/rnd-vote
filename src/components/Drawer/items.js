import { HowToVote, Poll } from "@mui/icons-material";

export const drawerItems = [
  {
    name: "Vote",
    icon: HowToVote,
    link: "/vote",
  },
  {
    name: "Results",
    icon: Poll,
    link: "/results",
  },
];
