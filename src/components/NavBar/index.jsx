"use client";
import * as React from "react";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Image from "next/image";
import ioclRndLogo from "/public/iocl-rnd.png";
import ioclRndLogoWhite from "/public/iocl-rnd-white.png";
import Link from "next/link";
import { Box, Tooltip, Menu, MenuItem } from "@/core-components/MuiComponents";
import { BackgroundLetterAvatars } from "@/core-components";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";
import Logout from "@mui/icons-material/Logout";
import Login from "@mui/icons-material/Login";
import LightMode from "@mui/icons-material/LightMode";
import DarkMode from "@mui/icons-material/DarkMode";
import { useTheme } from "next-themes";
import { useSession, signOut, signIn } from "next-auth/react";
import { APP_TITLE } from "@/enums/constants";

export default function NavBar(props) {
  const { data: session } = useSession();
  const { height, handleDrawerToggle, isSmallScreen } = props;
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const { theme, setTheme } = useTheme();

  const open = Boolean(anchorElUser);
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const handleLogout = () => {
    signOut();
    handleCloseUserMenu();
  };
  const handleLogin = () => {
    signIn();
    handleCloseUserMenu();
  };
  const toggleTheme = () => {
    if (theme === "light") {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  };
  return (
    <div
      className={`sticky left-0 top-0 z-50 flex flex-none basis-24 items-center justify-between gap-x-2 overflow-hidden bg-white p-2 shadow-md dark:bg-slate-700 sm:z-[1300]`}
    >
      <div className="flex flex-none items-center">
        <div className="flex items-center justify-center px-4">
          <IconButton
            aria-label="open drawer"
            edge="start"
            onClick={() => {
              handleDrawerToggle();
            }}
          >
            <MenuIcon />
          </IconButton>
        </div>
        {!isSmallScreen && (
          <Link href="/">
            <Image
              src={theme === "dark" ? ioclRndLogoWhite : ioclRndLogo}
              alt="IOCL R&D Logo"
              priority
            ></Image>
          </Link>
        )}
      </div>

      <div className="whitespace-nowrap bg-gradient-to-br from-red-500 to-blue-500 bg-clip-text font-poppins text-5xl font-bold text-transparent hover:from-red-600 hover:to-blue-700 dark:from-red-200 dark:to-blue-200 hover:dark:from-red-400 hover:dark:to-blue-400">
        <Link href="/">{APP_TITLE}</Link>
      </div>
      <div>
        <Box
          sx={{ display: "flex", alignItems: "center", textAlign: "center" }}
        >
          <Tooltip title="Account settings">
            <IconButton
              onClick={handleOpenUserMenu}
              aria-controls={open ? "account-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
            >
              <BackgroundLetterAvatars
                name={session?.user?.name}
                height={isSmallScreen ? 32 : 48}
                width={isSmallScreen ? 32 : 48}
              />
            </IconButton>
          </Tooltip>
        </Box>
        <Menu
          anchorEl={anchorElUser}
          id="account-menu"
          open={open}
          onClose={handleCloseUserMenu}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        >
          {session ? (
            <div>
              <div className="mx-2 mt-1 text-center text-sm">
                {session?.user?.name}
              </div>
              <div className="mx-2 mb-2 text-center text-sm">{`${
                session?.user?.designation || "NA"
              } (${session?.user?.username})`}</div>
              <Divider />
            </div>
          ) : (
            <div></div>
          )}
          <MenuItem onClick={toggleTheme}>
            {theme === "light" ? (
              <>
                <ListItemIcon>
                  <DarkMode fontSize="small" />
                </ListItemIcon>
                Dark Mode
              </>
            ) : (
              <>
                <ListItemIcon>
                  <LightMode fontSize="small" />
                </ListItemIcon>
                Light Mode
              </>
            )}
          </MenuItem>
          <MenuItem onClick={session ? handleLogout : handleLogin}>
            <ListItemIcon>
              {session ? (
                <Logout fontSize="small" />
              ) : (
                <Login fontSize="small" />
              )}
            </ListItemIcon>
            Logout
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
}
