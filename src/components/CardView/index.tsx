import { Chip } from "@mui/material";
import Icon from "@mui/material/Icon";
import Link from "next/link";
import { ElementType } from "react";

export default function CardView({
  title,
  link,
  icon,
  showChip,
  chipLabel,
  iconColor,
}: {
  title: string;
  link: string;
  icon: ElementType;
  showChip?: boolean;
  chipLabel?: number | string | undefined;
  iconColor?: string;
}) {
  return (
    <Link
      href={link}
      className={`flex basis-96 flex-col items-center justify-center gap-4 rounded-xl bg-white px-4 py-8 text-lg font-bold shadow-lg hover:bg-gradient-to-br hover:from-orange-400 hover:to-orange-600 hover:text-white hover:shadow-2xl dark:bg-slate-700`}
    >
      <Icon component={icon} style={{ color: iconColor }} />
      <div className="flex items-center gap-2">
        <span>{title}</span>
        {showChip && <Chip color="error" size="small" label={chipLabel} />}
      </div>
    </Link>
  );
}
