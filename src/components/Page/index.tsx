import { Icon } from "@mui/material";

export default function Page({
  title,
  icon,
  children,
}: {
  title: string;
  icon?: any;
  children: React.ReactNode;
}) {
  return (
    <div className="flex flex-1 flex-col items-center gap-8 p-8">
      <div className="flex justify-center gap-2 font-poppins text-3xl">
        {icon && <Icon fontSize="large" component={icon} />}
        <span>{title}</span>
      </div>
      {children}
    </div>
  );
}
