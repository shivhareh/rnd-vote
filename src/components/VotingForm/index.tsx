"use client";
import Page from "@/components/Page";
import { FormCheckbox, FormRadioGroup } from "@/core-components";
import { HowToVote } from "@mui/icons-material";
import { Button, Divider, FormHelperText } from "@mui/material";
import Checkbox from "@mui/material/Checkbox/Checkbox";
import FormControl from "@mui/material/FormControl/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel/FormControlLabel";
import FormGroup from "@mui/material/FormGroup/FormGroup";
import FormLabel from "@mui/material/FormLabel/FormLabel";
import Radio from "@mui/material/Radio/Radio";
import RadioGroup from "@mui/material/RadioGroup/RadioGroup";
import { useSession } from "next-auth/react";
import { RedirectType, redirect } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import submitResponse from "@/app/server-action";
import { revalidatePath } from "next/cache";
import {
  COORDINATOR_CANDIDATES,
  EC_MEMBER_CANDIDATES,
} from "@/enums/constants";

export default function VotingForm() {
  const { data: session } = useSession();
  const user = session?.user;
  const methods = useForm<any>({
    // defaultValues: {},
  });

  const [success, setSuccess] = useState(false);
  const [error, setError] = useState("");

  const {
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = methods;

  const formValues = watch();

  const ec_candidates_selected = [];
  for (const candidate of EC_MEMBER_CANDIDATES) {
    if (formValues[candidate.key]) {
      ec_candidates_selected.push(candidate.username);
    }
  }

  if (error) {
    return <p>{error}</p>;
  } else if (success) {
    return <p>Thank you for your response.</p>;
  }

  return (
    <div className=" flex w-1/2 flex-col gap-8 self-center bg-white p-8">
      <FormControl className="flex flex-col gap-2">
        <FormLabel id="employee-info-label">Employee:</FormLabel>
        <span>{`${user?.name}, ${user?.designation}`}</span>
      </FormControl>
      {/* <FormControl className="gap-2">
        <FormLabel id="coordinator-label">Co-ordinator</FormLabel>
        <FormRadioGroup
          className="gap-2"
          aria-labelledby="demo-radio-buttons-group-label"
          name="coordinator"
          control={control}
          rules={{ required: true }}
          error={errors?.coordinator}
        >
          {COORDINATOR_CANDIDATES.map((candidate) => (
            <>
              <FormControlLabel
                key={candidate.key}
                value={candidate.username}
                control={<Radio />}
                label={`[${candidate.username}] - ${candidate.name}`}
              />
              <Divider />
            </>
          ))}
        </FormRadioGroup>
      </FormControl> */}
      <FormControl
        className="gap-2"
        error={ec_candidates_selected.length !== 4}
      >
        <FormLabel id="ec-members-label">EC Members</FormLabel>
        <FormGroup className="gap-2">
          {EC_MEMBER_CANDIDATES.map((candidate) => (
            <>
              <FormControlLabel
                key={candidate.key}
                control={
                  <FormCheckbox name={candidate.key} control={control} />
                }
                label={`[${candidate.username}] - ${candidate.name}`}
              />
              <Divider />
            </>
          ))}
          {ec_candidates_selected.length !== 4 && (
            <FormHelperText>Please select four candidates.</FormHelperText>
          )}
        </FormGroup>
        <Button
          variant="contained"
          style={{ marginTop: 16 }}
          onClick={handleSubmit(async (data) => {
            const ec_candidates_selected = [];
            for (const candidate of EC_MEMBER_CANDIDATES) {
              if (data[candidate.key]) {
                ec_candidates_selected.push(candidate.username);
              }
            }

            if (ec_candidates_selected.length !== 4) {
              console.log("Please select four EC members.");
            } else {
              try {
                const formData = {
                  // @ts-expect-error
                  voterId: user?.username,
                  // coordinator: data.coordinator,
                  ec_members: ec_candidates_selected,
                };
                const response = await submitResponse(formData);
                console.log(response);
                // revalidatePath("/", "layout");
                setSuccess(true);
              } catch (e) {
                setError(e?.message);
                console.error(e);
              }
            }
          })}
        >
          Submit
        </Button>
      </FormControl>
    </div>
  );
}
